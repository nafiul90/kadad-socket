// const httpServer = require('http').createServer()
const express = require('express');
var cors = require('cors');
const { getOrderFromApi } = require('./orderController');
const { LIVE_DRIVER_ROOM } = require('./constant');
const {getDistanceFromLatLonInKm, isOrderCanceledByThisDriver} = require('./utils');
const app = express();

app.use(cors());

var PORT = process.env.PORT || 4000;
const server = app.listen(PORT, ()=> {
  console.log('server in started on', PORT);
})

// app.use("/hello", (req, res) => {
//   return res.status(200).send('Hi');
// })

const io = require('socket.io')(server, {
  cors: {origin: '*'}
});

// const io = require("socket.io")(8900, {
//   cors: {
//     origin: "*",
//   },
// });

let users = [];
let liveDrivers = [];
let orderCanceledDriverList = [];

const addUser = (userId, socketId) => {
  !users.some((user) => user.userId === userId) &&
    users.push({ userId, socketId });
};

const addDriver = (data, socketId) => {
  !liveDrivers.some((user) => user.userId === data.userId) &&
    liveDrivers.push({ ...data, socketId });
};

const removeUser = (socketId) => {
  users = users.filter((user) => user.socketId !== socketId);
};

const removeDriver = (socketId) => {
  liveDrivers = liveDrivers.filter((user) => user.socketId !== socketId);
};

const getUser = (userId) => {
  return users.find((user) => user.userId === userId);
};

io.on("connection", (socket) => {
  //when ceonnect
  console.log("a user connected.", socket.id);

  // chat start
  socket.on('message', data => {
    io.emit('message', data);
  })

  //take userId and socketId from user
  socket.on("addUser", (userId) => {
    console.log('user added ==> ', userId);
    addUser(userId, socket.id);
    io.emit("getUsers", users);
  });

  //send and get message
  socket.on("sendMessage", ({ senderId, receiverId, text }) => {
    console.log(senderId, receiverId, text)
    const user = getUser(receiverId);
    if(user){
      io.to(user.socketId).emit("getMessage", {
        senderId,
        text,
      });
    }
  });
  // chat end

  //order start
  socket.on("add-driver", (data) => {
    console.log('driver added ==> ', data);
    addDriver(data, socket.id);
    // driverLatLngList.push({id: data.userId, lat: data.lat, lng: data.lng});
    // socket.join(LIVE_DRIVER_ROOM);
    // const drivers = io.sockets.adapter.rooms.get(LIVE_DRIVER_ROOM);
    // console.log('live drivers ==> ',drivers);
    // io.emit("new-driver", drivers);
  });

  socket.on("lat-lng", (data) => {
    console.log(data);
    liveDrivers = liveDrivers.map( e => {
      if(e.userId == data.userId){
        e.lat = data.lat;
        e.lng = data.lng;
      }
      return e;
    });
  });

  socket.on('new-order', async (orderData) => {
    console.log('order data ==> ', orderData);
    const order = await getOrderFromApi(orderData);
    //check distance and create room
    for(let i=0; i< liveDrivers.length; i++){
      const driver = liveDrivers[i];
      const distance = getDistanceFromLatLonInKm(orderData.lat, orderData.lng, driver.lat, driver.lng);
      console.log('distance',distance);
      var clients = io.sockets.sockets;
      let socketList = Array.from(clients, ([name, value]) => ({ name, value }));
      // if( distance <= 2){
        if(!isOrderCanceledByThisDriver(driver.userId, orderData.orderId, orderCanceledDriverList)){
          let driverSocket = socketList.find( e => e.name == driver.socketId);
          driverSocket.value.join(LIVE_DRIVER_ROOM);
          // console.log("here")
          // io.to(driver.socketId).emit('new-order', order);
        }
      // }
    }

    io.to(LIVE_DRIVER_ROOM).emit('new-order', order)

  });

  socket.on('order-status', (data) => {
    console.log('order status has been changed ==> ', data);
    const customer = users.find( e => e.userId == data['customerId']);
    console.log('customer to send ==> ', customer);
    if(customer){
      io.to(customer.socketId).emit('order-status', data)
    }
  })

  socket.on('accept-order', (data) => {
    console.log('accept-order', data);
    io.to(data.customerSocketId).emit('accept-order', data);
  })

  socket.on('cancel-order', async (data) => {
    orderCanceledDriverList.push({orderId: data.orderId, driverId: data.driverId});
  });

  socket.on('end-order', async (data) => {
    orderCanceledDriverList = orderCanceledDriverList.filter( e => e.orderId != data.orderId);
  });

  //order end

  //when disconnect
  socket.on("disconnect", () => {
    console.log("a user disconnected!");
    removeUser(socket.id);
    removeDriver(socket.id);
    io.emit("getUsers", users);
  });
});


// httpServer.listen(port, function (err) {
//   if (err) console.log(err);
//   console.log('Listening on port', port);
// });
